# Git Workflow

We recommend using GitLab.

Our workflow is very similar (and greatly inspired) from [the official GitLab workflow](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/).

## Overview

Each project will be created with:

- an official repository under the BrainDot namespace,
- each contributor creates their own fork of it, under their own namespace.

## Setting up a new project

### Creating the official repository

Go to this page: [Create a new project on GitLab](https://gitlab.com/projects/new?namespace_id=4138431)

- The project name should be an accurate name, using normal casing.
- The project slug should follow kebab-case (lowercase words separated by dashes).
- The project description is mandatory and should describe the project acurately.

### Settings for the official repository

#### GitLab: Settings/General

In "Visibility, project features, permissions":

- The wiki should be disabled if it's not useful.

In "Merge requests":

- Merge method should be "Merge commit"
- Merge options:
  - Merge pipelines should try to validate the post-merge result
  - Diff discussions should NOT be resolved automatically when they become outdated
  - The CLI should link to the merge request URL
- Merge checks:
  - Pipelines must succeed
  - All discussions must be resolved

In "Merge requests approval": TODO

#### GitLab's Settings/Repository

The default branch should be `master`.

In "Push rules":

- Users can only push commits that were committed with one of their own verified emails.
- It is better, but not mandatory, to forbid unsigned commits.
- Users should not be allowed to remove tags.
- Prevent committing secrets

In "Protected branches":

- Developpers and maintainers should be allowed to merge to `master`
- No-one should be allowed to push to `master`

In "Protected tags":

- Tags of the form "v*" should be protected, and only maintainers should be able to create them.

#### GitLab's Settings/CI CD

TODO

## Clone the project

- Clone the project: `git clone URL`
- Go in the directory: `cd NAME`
- Rename the remote: `git remote rename origin braindot`
- Go to the project's page on GitLab, find the 'fork' button (top right). Create a fork in your own namespace.
- Copy the clone URL (top right)
- Add your fork as a remote: `git remote add YOUR-NAME URL`

This way:

- Because everyone is using their own repos, anyone can edit their history without risking to cause problems to other developpers (warning: you can still break your own version!)
- You can setup your own GitLab project as you wish
- Anyone can create a fork of the project, it is not needed to be added as a developper to the BrainDot team (external people can contribute).

To understand how to work with multiple remotes, we encourage you to read [this article from the Git SCM book](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes).

## How to contribute

### Overview

The workflow can be summarized into:

- Someone creates an issue,
- Someone implements it in their own fork,
- A Merge Request is submitted to BrainDot members,
- When the MR is accepted, the changes are merged to `master`.

The following sections will detail that behavior.

### Issues

Issues are used to list things that should be done, or bugs.

TODO

### Merge requests

TODO
